ESP Remote Control
==================

Small Project using an 433 MHz Remote Control, ESP8266 and MOS-Driver to control some LEDs.

Facts:
 - 433 MHz Remote - Modul RX480R_4CH
 - ESP 12-E Modul
 - 4x N-MOS Driver for the Leds
 - 5V USB powered with HT7333 to provide 3,3V for the ESP
 - 2x 4 channel Level Shifter for Remote Control and MOS Driver
