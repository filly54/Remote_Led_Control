/* 
 * Remote Ctrl
 */

#include<Arduino.h>
#include<Bounce2.h>
#include <ESP8266WiFi.h>


#define REMOTE_NO 4
#define OUTPUT_NO 4
#define PWM_STEPS 1024
#define DIM_STEPS 3

// Remote Input Pins
const int remotePin[REMOTE_NO] = {5, 4, 0, 2};
bool remoteState[REMOTE_NO] = {false, false, false, false};
// array of debouncer objects
Bounce debouncer[REMOTE_NO] = Bounce();

// Output Pins
const int outputPin[OUTPUT_NO] = {14, 12, 13, 15};
int pwm[OUTPUT_NO] = {0, 0, 0, 0};



// Network Settings
const char *ssid = "Andis_Light_Network";
const char *password = "backtocuba";

// HTTP Server
WiFiServer server(80);
unsigned long ulReqcount;
unsigned long ulReconncount;

/* Just a little test message.  Go to http://192.168.4.1 in a web browser
   connected to this access point to see it.
   */

void setup()
{
    delay(1000);
    Serial.begin(115200);
    Serial.println();
    Serial.print("Configuring access point...");
    /* You can remove the password parameter if you want the AP to be open. */
    WiFi.softAP(ssid, password);

    IPAddress myIP = WiFi.softAPIP();
    Serial.print("AP IP address: ");
    Serial.println(myIP);
    server.begin();
    Serial.println("HTTP server started");
    Serial.println("Remote Ctrl booting\n");

    for (int i = 0; i < REMOTE_NO; i++)
        pinMode(remotePin[i], INPUT);

    for (int i = 0; i < REMOTE_NO; i++)
    {
        debouncer[i].attach(remotePin[i]);
        debouncer[i].interval(50);
    }

    for (int i = 0; i < OUTPUT_NO; i++)
    {
        pinMode(outputPin[i], OUTPUT);
        digitalWrite(outputPin[i], LOW);
    }
}

bool checkRemoteState(int idx)
{
    bool output = false;

    debouncer[idx].update();

    if (debouncer[idx].rose() == true)
    {
        Serial.println("Remote pushed!");
        output = true;
    }

    return output;
}

void setOutput(int idx, bool state)
{
    if (state)
    {
        if (pwm[idx] < ((PWM_STEPS - 1) / DIM_STEPS))
            pwm[idx] = ((PWM_STEPS - 1) / DIM_STEPS);

        analogWrite(outputPin[idx], pwm[idx]);
    }
    else
        analogWrite(outputPin[idx], 0);
}

void incOutputPwm(int idx)
{
    pwm[idx] = pwm[idx] + ((PWM_STEPS - 1) / DIM_STEPS);

    if (pwm[idx] > PWM_STEPS - 1)
        pwm[idx] = 0;//((PWM_STEPS - 1) / DIM_STEPS);

    analogWrite(outputPin[idx], pwm[idx]);
}

void runHttpServer()
{
    // Check if a client has connected
    WiFiClient client = server.available();
    if (!client) 
    {
        return;
    }

    // Wait until the client sends some data
    Serial.println("new client");
    unsigned long ultimeout = millis()+250;
    while(!client.available() && (millis()<ultimeout) )
    {
        delay(1);
    }
    if(millis()>ultimeout) 
    { 
        Serial.println("client connection time-out!");
        return; 
    }

    // Read the first line of the request
    String sRequest = client.readStringUntil('\r');
    //Serial.println(sRequest);
    client.flush();

    // stop client, if request is empty
    if(sRequest=="")
    {
        Serial.println("empty request! - stopping client");
        client.stop();
        return;
    }

    // get path; end of path is either space or ?
    // Syntax is e.g. GET /?pin=MOTOR1STOP HTTP/1.1
    String sPath="",sParam="", sCmd="";
    String sGetstart="GET ";
    int iStart,iEndSpace,iEndQuest;
    iStart = sRequest.indexOf(sGetstart);
    if (iStart>=0)
    {
        iStart+=+sGetstart.length();
        iEndSpace = sRequest.indexOf(" ",iStart);
        iEndQuest = sRequest.indexOf("?",iStart);

        // are there parameters?
        if(iEndSpace>0)
        {
            if(iEndQuest>0)
            {
                // there are parameters
                sPath  = sRequest.substring(iStart,iEndQuest);
                sParam = sRequest.substring(iEndQuest,iEndSpace);
            }
            else
            {
                // NO parameters
                sPath  = sRequest.substring(iStart,iEndSpace);
            }
        }
    }

    ///////////////////////////////////////////////////////////////////////////////
    // output parameters to serial, you may connect e.g. an Arduino and react on it
    ///////////////////////////////////////////////////////////////////////////////
    if(sParam.length()>0)
    {
        int iEqu=sParam.indexOf("=");
        if(iEqu>=0)
        {
            sCmd = sParam.substring(iEqu+1,sParam.length());
            Serial.println(sCmd);
        }
    }


    ///////////////////////////
    // format the html response
    ///////////////////////////
    String sResponse,sHeader;

    ////////////////////////////
    // 404 for non-matching path
    ////////////////////////////
    if(sPath!="/")
    {
        sResponse="<html><head><title>404 Not Found</title></head><body><h1>Not Found</h1><p>The requested URL was not found on this server.</p></body></html>";

        sHeader  = "HTTP/1.1 404 Not found\r\n";
        sHeader += "Content-Length: ";
        sHeader += sResponse.length();
        sHeader += "\r\n";
        sHeader += "Content-Type: text/html\r\n";
        sHeader += "Connection: close\r\n";
        sHeader += "\r\n";
    }
    ///////////////////////
    // format the html page
    ///////////////////////
    else
    {
        ulReqcount++;
        sResponse  = "<html><head><title>Andis Lichtsteuerung</title></head><body>";
        sResponse += "<font color=\"#000000\"><body bgcolor=\"#d0d0f0\">";
        sResponse += "<meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0, user-scalable=yes\">";
        sResponse += "<h1>Andis Lichtsteuerung</h1>";
        sResponse += "<FONT SIZE=+1>";
        sResponse += "<p>Ausgang 0 <a href=\"?pin=OUTPUT0OFF\"><button>ausschalten</button></a>&nbsp;<a href=\"?pin=OUTPUT0ON\"><button>einschalten</button></a>&nbsp;<a href=\"?pin=OUTPUT0PWMINC\"><button>helligkeit</button></a></p>";
        sResponse += "<p>Ausgang 1 <a href=\"?pin=OUTPUT1OFF\"><button>ausschalten</button></a>&nbsp;<a href=\"?pin=OUTPUT1ON\"><button>einschalten</button></a>&nbsp;<a href=\"?pin=OUTPUT1PWMINC\"><button>helligkeit</button></a></p>";
        sResponse += "<p>Ausgang 2 <a href=\"?pin=OUTPUT2OFF\"><button>ausschalten</button></a>&nbsp;<a href=\"?pin=OUTPUT2ON\"><button>einschalten</button></a>&nbsp;<a href=\"?pin=OUTPUT2PWMINC\"><button>helligkeit</button></a></p>";
        sResponse += "<p>Ausgang 3 <a href=\"?pin=OUTPUT3OFF\"><button>ausschalten</button></a>&nbsp;<a href=\"?pin=OUTPUT3ON\"><button>einschalten</button></a>&nbsp;<a href=\"?pin=OUTPUT3PWMINC\"><button>helligkeit</button></a></p>";

        //////////////////////
        // react on parameters
        //////////////////////
        if (sCmd.length()>0)
        {
            // write received command to html page
            sResponse += "Kommando:" + sCmd + "<BR>"; 
            // switch GPIO
            if(sCmd.indexOf("OUTPUT0ON")>=0)
                setOutput(0, true);
            else if(sCmd.indexOf("OUTPUT0OFF")>=0)
                setOutput(0, false);
            else if(sCmd.indexOf("OUTPUT0PWMINC")>=0)
                incOutputPwm(0);

            if(sCmd.indexOf("OUTPUT1ON")>=0)
                setOutput(1, true);
            else if(sCmd.indexOf("OUTPUT1OFF")>=0)
                setOutput(1, false);
            else if(sCmd.indexOf("OUTPUT1PWMINC")>=0)
                incOutputPwm(1);

            if(sCmd.indexOf("OUTPUT2ON")>=0)
                setOutput(2, true);
            else if(sCmd.indexOf("OUTPUT2OFF")>=0)
                setOutput(2, false);
            else if(sCmd.indexOf("OUTPUT2PWMINC")>=0)
                incOutputPwm(2);

            if(sCmd.indexOf("OUTPUT3ON")>=0)
                setOutput(3, true);
            else if(sCmd.indexOf("OUTPUT3OFF")>=0)
                setOutput(3, false);
            else if(sCmd.indexOf("OUTPUT3PWMINC")>=0)
                incOutputPwm(3);
        }

        sResponse += "<FONT SIZE=-2>";
        sResponse += "<BR>Aufrufz&auml;hler="; 
        sResponse += ulReqcount;
        sResponse += " - Verbindungsz&auml;hler="; 
        sResponse += ulReconncount;
        sResponse += "<BR>";
        sResponse += "Philipp Schreiber 06/2018<BR>";
        sResponse += "</body></html>";

        sHeader  = "HTTP/1.1 200 OK\r\n";
        sHeader += "Content-Length: ";
        sHeader += sResponse.length();
        sHeader += "\r\n";
        sHeader += "Content-Type: text/html\r\n";
        sHeader += "Connection: close\r\n";
        sHeader += "\r\n";
    }

    // Send the response to the client
    client.print(sHeader);
    client.print(sResponse);

    // and stop the client
    client.stop();
    Serial.println("Client disonnected");
}


void loop()
{
    // Check all Inputs
    for (int i = 0; i < REMOTE_NO; i++)
    {
        if (checkRemoteState(i))
            incOutputPwm(i);
    }

    runHttpServer();
}
